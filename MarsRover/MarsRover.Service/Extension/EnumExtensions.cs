using System;
using System.ComponentModel;
using System.Reflection;

namespace MarsRover.Service.Extension
{
    public static class EnumExtensions
    {
        public static string GetEnumDescription(this Enum value)
        {
            Type enumType = value.GetType();

            FieldInfo field = enumType.GetField(value.ToString());

            object[] attributes = field.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attributes.Length == 0 ? value.ToString() : ((DescriptionAttribute)attributes[0]).Description;
        }

        public static TR GetEnumFromDescription<TR>(this string description)
        {
            foreach (FieldInfo field in typeof(TR).GetFields())
            {
                DescriptionAttribute attribute
                    = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attribute == null)
                    continue;
                if (attribute.Description == description)
                {
                    return (TR)field.GetValue(null);
                }
            }
            return default(TR);
        }
    }
}
