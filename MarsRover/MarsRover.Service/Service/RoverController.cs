using MarsRover.Service.Domain;
using System.Collections.Generic;
using System.Linq;

namespace MarsRover.Service.Service
{
    public class RoverController
    {
        private readonly IRoverInfoParser _roverInfoParser;

        public RoverController(IRoverInfoParser roverInfoParser)
        {
            _roverInfoParser = roverInfoParser;
        }

        public string[] MoveRovers(string[] roverData)
        {
            IList<RoverInfo> rovers = _roverInfoParser.CreateRovers(roverData);

            foreach (RoverInfo roverInfo in rovers)
            {
                Rover rover = roverInfo.Rover;
                rover.Move(roverInfo.Movements);
            }

            return rovers.Select(r => r.Rover.ToString()).ToArray();
        }
    }
}
