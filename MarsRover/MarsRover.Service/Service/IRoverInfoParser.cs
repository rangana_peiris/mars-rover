using MarsRover.Service.Domain;
using System.Collections.Generic;

namespace MarsRover.Service.Service
{
    public interface IRoverInfoParser
    {
        IList<RoverInfo> CreateRovers(string[] roverData);
    }
}