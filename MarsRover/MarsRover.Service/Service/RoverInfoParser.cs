using MarsRover.Service.Domain;
using MarsRover.Service.Extension;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MarsRover.Service.Service
{
    public class RoverInfoParser : IRoverInfoParser
    {
        private const int MinimumNumberOfRecordsToProceed = 3;
        private const int MinimumNumberOfEntriesPerRoverLocation = 3;

        public IList<RoverInfo> CreateRovers(string[] roverData)
        {
            if (roverData == null || !roverData.Any() || roverData.Length < MinimumNumberOfRecordsToProceed)
            {
                throw new ArgumentException("No rover data");
            }

            Plateau plateau = GetPlateau(roverData[0]);

            IList<RoverInfo> roverInfos = new List<RoverInfo>();
            for (int i = 1; i < roverData.Length; i += 2) // Skip first record in rover data
            {
                if (i + 1 >= roverData.Length) break;

                RoverInfo roverInfo = BuildRoverInfo(plateau, roverData[i], roverData[i + 1]);
                roverInfos.Add(roverInfo);
            }

            return roverInfos;
        }

        private Plateau GetPlateau(string roverPlateauDimensions)
        {
            string[] plateauDimensions = roverPlateauDimensions.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            if (plateauDimensions == null ||
                plateauDimensions.Length != 2 ||
                !int.TryParse(plateauDimensions[0], out int width) ||
                width < 1 ||
                !int.TryParse(plateauDimensions[1], out int height) ||
                height < 1)
            {
                throw new ArgumentException("Invalid rover plateau dimensions");
            }

            return new Plateau(width, height);
        }

        private RoverInfo BuildRoverInfo(Plateau plateau, string initialLocationAndDirection, string sequence)
        {
            string[] initial = initialLocationAndDirection.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            if (initial == null ||
                initial.Length != MinimumNumberOfEntriesPerRoverLocation)
            {
                throw new ArgumentException("Invalid rover location data");
            }

            Location location = BuildLocation(initial[0], initial[1]);
            Direction direction = GetDirection(initial[2]);
            IList<Movement> movements = BuildMovements(sequence);

            return new RoverInfo(new Rover(plateau, location, direction), movements);
        }

        private Location BuildLocation(string xPos, string yPos)
        {
            if (!int.TryParse(xPos, out int x) ||
                x < 0 ||
                !int.TryParse(yPos, out int y) ||
                y < 0)
            {
                throw new ArgumentException("Invalid rover location data");
            }

            return new Location(x, y);
        }

        private Direction GetDirection(string directionVal)
        {
            Direction direction = directionVal.GetEnumFromDescription<Direction>();

            if (direction == Direction.Unknown)
            {
                throw new ArgumentException("Invalid rover initial direction");
            }

            return direction;
        }

        private IList<Movement> BuildMovements(string sequence)
        {
            return sequence
                    .Select(s => s.ToString().GetEnumFromDescription<Movement>())
                    .Where(d => d != Movement.Unknown)
                    .ToList();
        }
    }
}