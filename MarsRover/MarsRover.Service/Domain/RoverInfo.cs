using System.Collections.Generic;

namespace MarsRover.Service.Domain
{
    public class RoverInfo
    {
        public Rover Rover { get; }

        public IList<Movement> Movements { get; }

        public RoverInfo(Rover rover, IList<Movement> movements)
        {
            Rover = rover;
            Movements = movements;
        }
    }
}