using System.ComponentModel;

namespace MarsRover.Service.Domain
{
    public enum Movement
    {
        [Description("U")]
        Unknown = 0,

        [Description("L")]
        Left = 1,

        [Description("R")]
        Right = 2,

        [Description("M")]
        Move = 3
    }
}