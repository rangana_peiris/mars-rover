using MarsRover.Service.Extension;
using System;
using System.Collections.Generic;

namespace MarsRover.Service.Domain
{
    public class Rover
    {
        public Plateau Plateau { get; }

        public Location Location { get; }

        public Direction Direction { get; private set; }

        public Rover(Plateau plateau, Location initialLocation, Direction direction)
        {
            Plateau = plateau;

            if (!IsLocationValid(initialLocation.X, initialLocation.Y))
            {
                throw new InvalidOperationException("Rover is placed outside of plateau");
            }

            Location = initialLocation;
            Direction = direction;
        }

        public string Move(IEnumerable<Movement> movements)
        {
            if (movements == null) return null;

            foreach (Movement movement in movements)
            {
                switch (movement)
                {
                    case Movement.Left:
                        TurnLeft();
                        break;
                    case Movement.Right:
                        TurnRight();
                        break;
                    case Movement.Move:
                        Move();
                        break;
                }
            }

            return ToString();
        }

        public override string ToString()
        {
            return $"{Location.X} {Location.Y} {Direction.GetEnumDescription()}";
        }

        private void TurnLeft()
        {
            switch (Direction)
            {
                case Direction.North: Direction = Direction.West; break;
                case Direction.West: Direction = Direction.South; break;
                case Direction.South: Direction = Direction.East; break;
                case Direction.East: Direction = Direction.North; break;
            }
        }

        private void TurnRight()
        {
            switch (Direction)
            {
                case Direction.North: Direction = Direction.East; break;
                case Direction.East: Direction = Direction.South; break;
                case Direction.South: Direction = Direction.West; break;
                case Direction.West: Direction = Direction.North; break;
            }
        }

        private void Move()
        {
            int newX = Location.X;
            int newY = Location.Y;

            switch (Direction)
            {
                case Direction.North: newY = Location.Y + 1; break;
                case Direction.East: newX = Location.X + 1; break;
                case Direction.South: newY = Location.Y - 1; break;
                case Direction.West: newX = Location.X - 1; break;
            }

            // TODO - Decide what to do if rover try to escape out of plateau - Right now rover won't move out of plateau (calm and quite)
            if (!IsLocationValid(newX, newY)) return;

            Location.X = newX;
            Location.Y = newY;
        }

        private bool IsLocationValid(int x, int y)
        {
            return x >= 0 && x <= Plateau.Width && y >= 0 && y <= Plateau.Height;
        }
    }
}