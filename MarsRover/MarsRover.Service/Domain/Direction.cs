using System.ComponentModel;

namespace MarsRover.Service.Domain
{
    public enum Direction
    {
        [Description("U")]
        Unknown = 0,

        [Description("N")]
        North = 1,

        [Description("E")]
        East = 2,

        [Description("S")]
        South = 3,

        [Description("W")]
        West = 4
    }
}