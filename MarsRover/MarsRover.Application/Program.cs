using MarsRover.Service.Service;
using System;
using System.Linq;

namespace MarsRover.Application
{
    class Program
    {
        public static void Main(string[] roverData)
        {
            if (roverData != null && roverData.Any())
            {
                RoverController roverController = new RoverController(new RoverInfoParser());

                try
                {
                    string[] roverPositions = roverController.MoveRovers(roverData);

                    if (roverPositions == null || !roverPositions.Any())
                    {
                        Console.WriteLine("No rover position found");
                    }
                    else
                    {
                        foreach (string position in roverPositions)
                        {
                            Console.WriteLine(position);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error during processing");
                }
            }

            Console.Read();
        }
    }
}
