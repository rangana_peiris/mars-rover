using MarsRover.Service.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace MarsRover.Test
{
    [TestClass]
    public class RoverControllerTest
    {
        [TestMethod]
        public void Should_Give_Rover_Positions_After_Navigation_When_Valid()
        {
            // Arrange
            RoverController roverController = new RoverController(new RoverInfoParser());
            string[] roverData =
            {
                "5 5",
                "1 2 N",
                "LMLMLMLMM",
                "3 3 E",
                "MMRMMRMRRM"
            };

            // Act
            string[] roverPositions = roverController.MoveRovers(roverData);

            // Assert
            Assert.IsNotNull(roverPositions);
            Assert.AreEqual(2, roverPositions.Length);
            Assert.AreEqual("1 3 N", roverPositions[0]);
            Assert.AreEqual("5 1 E", roverPositions[1]);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_Give_No_Rover_Positions_When_No_Rovers()
        {
            // Arrange
            RoverController roverController = new RoverController(new RoverInfoParser());
            string[] roverData =
            {
                "5 5"
            };

            // Act
            string[] roverPositions = roverController.MoveRovers(roverData);

            // Assert
        }

        [TestMethod]
        public void Should_Give_Rover_Position_As_Initial_When_No_Movement_Given()
        {
            // Arrange
            RoverController roverController = new RoverController(new RoverInfoParser());
            string[] roverData =
            {
                "5 5",
                "1 2 N",
                ""
            };

            // Act
            string[] roverPositions = roverController.MoveRovers(roverData);

            // Assert
            Assert.IsNotNull(roverPositions);
            Assert.AreEqual(1, roverPositions.Length);
            Assert.AreEqual("1 2 N", roverPositions[0]);
        }

        [TestMethod]
        public void Should_Give_Rover_Position_As_Initial_When_Invalid_Movement_Given()
        {
            // Arrange
            RoverController roverController = new RoverController(new RoverInfoParser());
            string[] roverData =
            {
                "5 5",
                "1 2 N",
                "XXBFDA"
            };

            // Act
            string[] roverPositions = roverController.MoveRovers(roverData);

            // Assert
            Assert.IsNotNull(roverPositions);
            Assert.AreEqual(1, roverPositions.Length);
            Assert.AreEqual("1 2 N", roverPositions[0]);
        }
    }
}
