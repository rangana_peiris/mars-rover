using MarsRover.Service.Domain;
using MarsRover.Service.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MarsRover.Test
{
    [TestClass]
    public class RoverInfoParserTest
    {
        [TestMethod]
        public void Should_Parse_Rover_Data_When_Valid()
        {
            // Arrange
            IRoverInfoParser roverInfoParser = new RoverInfoParser();
            string[] roverData =
            {
                "5 5",
                "1 2 N",
                "LMLMLMLMM",
                "3 3 E",
                "MMRMMRMRRM"
            };

            // Act
            IList<RoverInfo> rovers = roverInfoParser.CreateRovers(roverData);

            // Assert
            Assert.AreEqual(2, rovers.Count);

            RoverInfo roverInfo1 = rovers.First();
            Assert.AreEqual(roverInfo1.Rover.Plateau.Width, 5);
            Assert.AreEqual(roverInfo1.Rover.Plateau.Height, 5);
            Assert.AreEqual(9, roverInfo1.Movements.Count);
            Assert.AreEqual(roverInfo1.Rover.Location.X, 1);
            Assert.AreEqual(roverInfo1.Rover.Location.Y, 2);
            Assert.AreEqual(roverInfo1.Rover.Direction, Direction.North);

            RoverInfo roverInfo2 = rovers.Last();
            Assert.AreEqual(roverInfo2.Rover.Plateau.Width, 5);
            Assert.AreEqual(roverInfo2.Rover.Plateau.Height, 5);
            Assert.AreEqual(10, roverInfo2.Movements.Count);
            Assert.AreEqual(roverInfo2.Rover.Location.X, 3);
            Assert.AreEqual(roverInfo2.Rover.Location.Y, 3);
            Assert.AreEqual(roverInfo2.Rover.Direction, Direction.East);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_Throw_Error_When_Null_Rover_Data()
        {
            // Arrange
            IRoverInfoParser roverInfoParser = new RoverInfoParser();
            string[] roverData = null;

            // Act
            IList<RoverInfo> rovers = roverInfoParser.CreateRovers(roverData);

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_Throw_Error_When_No_Rover_Data()
        {
            // Arrange
            IRoverInfoParser roverInfoParser = new RoverInfoParser();
            string[] roverData = new string[0];

            // Act
            IList<RoverInfo> rovers = roverInfoParser.CreateRovers(roverData);

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_Throw_Error_When_AtLeast_No_Minium_Rover_Data()
        {
            // Arrange
            IRoverInfoParser roverInfoParser = new RoverInfoParser();
            string[] roverData =
            {
                "5 5",
                "1 2 N"
            };

            // Act
            IList<RoverInfo> rovers = roverInfoParser.CreateRovers(roverData);

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_Throw_Error_When_Invalid_Rover_Palteau_Data_Case_1()
        {
            // Arrange
            IRoverInfoParser roverInfoParser = new RoverInfoParser();
            string[] roverData =
            {
                "5 ",
                "1 2 N",
                "LMMMMRR"
            };

            // Act
            IList<RoverInfo> rovers = roverInfoParser.CreateRovers(roverData);

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_Throw_Error_When_Invalid_Rover_Palteau_Data_Case_2()
        {
            // Arrange
            IRoverInfoParser roverInfoParser = new RoverInfoParser();
            string[] roverData =
            {
                "5 x",
                "1 2 N",
                "LMMMMRR"
            };

            // Act
            IList<RoverInfo> rovers = roverInfoParser.CreateRovers(roverData);

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_Throw_Error_When_Invalid_Rover_Palteau_Data_Case_3()
        {
            // Arrange
            IRoverInfoParser roverInfoParser = new RoverInfoParser();
            string[] roverData =
            {
                "-1 5",
                "1 2 N",
                "LMMMMRR"
            };

            // Act
            IList<RoverInfo> rovers = roverInfoParser.CreateRovers(roverData);

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_Throw_Error_When_Invalid_Rover_Postion_Data_Case_1()
        {
            // Arrange
            IRoverInfoParser roverInfoParser = new RoverInfoParser();
            string[] roverData =
            {
                "5 5",
                "1 N",
                "LMMMMRR"
            };

            // Act
            IList<RoverInfo> rovers = roverInfoParser.CreateRovers(roverData);

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_Throw_Error_When_Invalid_Rover_Postion_Data_Case_2()
        {
            // Arrange
            IRoverInfoParser roverInfoParser = new RoverInfoParser();
            string[] roverData =
            {
                "5 5",
                "x 2 N",
                "LMMMMRR"
            };

            // Act
            IList<RoverInfo> rovers = roverInfoParser.CreateRovers(roverData);

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_Throw_Error_When_Invalid_Rover_Postion_Data_Case_3()
        {
            // Arrange
            IRoverInfoParser roverInfoParser = new RoverInfoParser();
            string[] roverData =
            {
                "5 5",
                "1 -2 N",
                "LMMMMRR"
            };

            // Act
            IList<RoverInfo> rovers = roverInfoParser.CreateRovers(roverData);

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_Throw_Error_When_Invalid_Rover_Direction()
        {
            // Arrange
            IRoverInfoParser roverInfoParser = new RoverInfoParser();
            string[] roverData =
            {
                "5 5",
                "1 2 X",
                "LMMMMRR"
            };

            // Act
            IList<RoverInfo> rovers = roverInfoParser.CreateRovers(roverData);

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_Throw_Error_When_Null_Rover_Sequence_Data()
        {
            // Arrange
            IRoverInfoParser roverInfoParser = new RoverInfoParser();
            string[] roverData =
            {
                "5 5",
                "1 2 N"
            };

            // Act
            IList<RoverInfo> rovers = roverInfoParser.CreateRovers(roverData);

            // Assert
        }

        [TestMethod]
        public void Should_Parse_Rover_Data_When_Empty_Sequence_Data()
        {
            // Arrange
            IRoverInfoParser roverInfoParser = new RoverInfoParser();
            string[] roverData =
            {
                "5 5",
                "1 2 N",
                ""
            };

            // Act
            IList<RoverInfo> rovers = roverInfoParser.CreateRovers(roverData);

            // Assert
            Assert.AreEqual(1, rovers.Count);

            RoverInfo roverInfo1 = rovers.First();
            Assert.AreEqual(roverInfo1.Rover.Plateau.Width, 5);
            Assert.AreEqual(roverInfo1.Rover.Plateau.Height, 5);
            Assert.AreEqual(0, roverInfo1.Movements.Count);
            Assert.AreEqual(roverInfo1.Rover.Location.X, 1);
            Assert.AreEqual(roverInfo1.Rover.Location.Y, 2);
            Assert.AreEqual(roverInfo1.Rover.Direction, Direction.North);
        }

        [TestMethod]
        public void Should_Parse_Rover_Data_When_Rover_Sequence_Has_Invalid_Data()
        {
            // Arrange
            IRoverInfoParser roverInfoParser = new RoverInfoParser();
            string[] roverData =
            {
                "5 5",
                "1 2 N",
                "LXMMMMRRYY"
            };

            // Act
            IList<RoverInfo> rovers = roverInfoParser.CreateRovers(roverData);

            // Assert
            Assert.AreEqual(1, rovers.Count);

            RoverInfo roverInfo1 = rovers.First();
            Assert.AreEqual(roverInfo1.Rover.Plateau.Width, 5);
            Assert.AreEqual(roverInfo1.Rover.Plateau.Height, 5);
            Assert.AreEqual(7, roverInfo1.Movements.Count);
            Assert.AreEqual(roverInfo1.Rover.Location.X, 1);
            Assert.AreEqual(roverInfo1.Rover.Location.Y, 2);
            Assert.AreEqual(roverInfo1.Rover.Direction, Direction.North);
        }
    }
}
