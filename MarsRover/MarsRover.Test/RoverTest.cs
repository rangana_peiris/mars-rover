using MarsRover.Service.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace MarsRover.Test
{
    [TestClass]
    public class RoverTest
    {
        [TestMethod]
        public void Should_Roam_When_Sequence_Is_Valid_Case_1()
        {
            // Arrange
            Rover rover = new Rover(new Plateau(5, 5), new Location(1, 2), Direction.North);

            // Act - LMLMLMLMM
            string currentRoverLocation = rover.Move(new []
            {
                Movement.Left, Movement.Move, Movement.Left, Movement.Move, Movement.Left,
                Movement.Move, Movement.Left, Movement.Move, Movement.Move
            });
             
            // Assert - 1 3 N
            Assert.AreEqual("1 3 N", currentRoverLocation);
        }

        [TestMethod]
        public void Should_Roam_When_Sequence_Is_Valid_Case_2()
        {
            // Arrange
            Rover rover = new Rover(new Plateau(5, 5), new Location(3, 3), Direction.East);

            // Act - MMRMMRMRRM
            string currentRoverLocation = rover.Move(new[]
            {
                Movement.Move, Movement.Move, Movement.Right, Movement.Move, Movement.Move,
                Movement.Right, Movement.Move, Movement.Right, Movement.Right, Movement.Move
            });

            // Assert - 5 1 E
            Assert.AreEqual("5 1 E", currentRoverLocation);
        }

        [TestMethod]
        public void Should_Not_Roam_When_Sequence_Is_Null()
        {
            // Arrange
            Rover rover = new Rover(new Plateau(5, 5), new Location(3, 3), Direction.East);

            // Act
            string currentRoverLocation = rover.Move(null);

            // Assert
            Assert.IsNull(currentRoverLocation);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Should_Throw_Error_When_Initial_Rover_Location_Is_Invalid_Case_1()
        {
            // Arrange
            Rover rover = new Rover(new Plateau(5, 5), new Location(5, 6), Direction.East);

            // Act

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Should_Throw_Error_When_Initial_Rover_Location_Is_Invalid_Case_2()
        {
            // Arrange
            Rover rover = new Rover(new Plateau(5, 5), new Location(6, 5), Direction.East);

            // Act

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Should_Throw_Error_When_Initial_Rover_Location_Is_Invalid_Case_3()
        {
            // Arrange
            Rover rover = new Rover(new Plateau(5, 5), new Location(6, 6), Direction.East);

            // Act

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Should_Throw_Error_When_Initial_Rover_Location_Is_Invalid_Case_4()
        {
            // Arrange
            Rover rover = new Rover(new Plateau(5, 5), new Location(-1, 1), Direction.East);

            // Act

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Should_Throw_Error_When_Initial_Rover_Location_Is_Invalid_Case_5()
        {
            // Arrange
            Rover rover = new Rover(new Plateau(5, 5), new Location(1, -1), Direction.East);

            // Act

            // Assert
        }

        [TestMethod]
        public void Should_Roam_When_Sequence_Try_To_Cross_Plateau_Boundary()
        {
            // Arrange
            Rover rover = new Rover(new Plateau(5, 5), new Location(4, 4), Direction.East);

            // Act - MMMMRM
            string currentRoverLocation = rover.Move(new[]
            {
                Movement.Move, Movement.Move, Movement.Move, Movement.Move, Movement.Right, Movement.Move
            });

            // Assert - 5 3 S
            Assert.AreEqual("5 3 S", currentRoverLocation);
        }
    }
}
