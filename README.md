# README #

The mars rover application to find final location of rovers

### What is this repository for? ###

This solution provides a mecahnism to move the rovers in mars and find the eventual location

The application is developed with Visual Studio 2017 Professional edition with .Net Framework 4.6.1.

This repository is public.

### How do I get set up? ###

1. First clone the source code from the Bitbucket

2. Open the 'MarsRover' folder and open the 'MarsRover.sln' in Visual Studio 2017

3. Build the solution

4. Go to the bin directory of the 'MarsRover.Application' project on command line

5. Execute the following command with given sample arguments

   MarsRover.Application "5 5" "1 2 N" "LMLMLMLMM" "3 3 E" "MMRMMRMRRM"
  
### Notes ###

1. All possible unit test cases may not be covered, as there are many in terms of input validation
   
### Assumptions ###

1. Even the rovers move sequentially, nothing is mentioned on what happen after one finishes the given path navigation. 
   Here I assume that rover is taken out of surface after the navigation to avoid collision with next rover provided the path overlaps are found.
   